package com.example.root.fusedlocationtestapplication;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import timber.log.Timber;

public class Util {

    public static String SERVICE_ALARM_INTENT = "Tracking.Alarm.Intent";
    public static String CHANNEL_TRACKING_ID = "channelTrackingId";

    public static Notification getNotification(Context context) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, getMainActivityIntent(context), 0);
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        return new NotificationCompat.Builder(context, CHANNEL_TRACKING_ID)
                .setContentTitle(context.getString(R.string.notification_rastreamento_title))
                .setTicker(context.getString(R.string.notification_rastreamento_title))
                .setContentText(context.getString(R.string.notificacao_rastreamento_text))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                .setOngoing(true)
                .setLargeIcon(icon)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setColorized(true)
                .setColor(context.getResources().getColor(R.color.colorAccent))
                .build();
    }

    private static Intent getMainActivityIntent(Context context) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return notificationIntent;
    }

    public static boolean isAndroidO() {
        boolean isO = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
        Timber.e("Is Android O %s", isO);
        return isO;
    }

    public static void saveOnLogFile(String messageToSave) {
        Timber.e("isExternalStorageWritable: %s", isExternalStorageWritable());
        if (isExternalStorageWritable()) {
            File logDirectory = new File(Environment.getExternalStorageDirectory() + "/FusedLocationTest");

            if (!logDirectory.exists()) logDirectory.mkdirs();

            File logFile = new File(logDirectory, "fused_location_test.txt");
            try {
                BufferedWriter bfWriter = new BufferedWriter(new FileWriter(logFile, true));
                bfWriter.append(messageToSave);
                bfWriter.flush();
                bfWriter.close();
            } catch (IOException ioException) {
                Timber.e("saveError LOGFILE IOEXCEPTION -> " + ioException.getMessage());
            } catch (Exception e) {
                Timber.e("saveError LOGFILE EXCEPTION -> " + e.getMessage());
            }
        } else {
            Timber.e("saveError LOGFILE --- NÃO POSSUI EXTERNAL STORAGE");
        }
    }

    public static boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    @SuppressLint("NewApi")
    public static void startTrackingService(Context context) {
        Timber.e("startTrackingService");
        Intent trackingIntent = new Intent(context, TrackingService.class);
        if(isAndroidO()) context.startForegroundService(trackingIntent);
        else context.startService(trackingIntent);
    }
}
