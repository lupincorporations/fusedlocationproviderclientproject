package com.example.root.fusedlocationtestapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.AlarmManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainActivity extends AppCompatActivity {

    private TextView mLocationLogTv;
    private String PERMISSION_GRANTED = "PERMISSION_GRANTED";

    private BroadcastReceiver mainReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null) if(intent.getAction().equals(PERMISSION_GRANTED)) Util.startTrackingService(context);
        }
    };
    private boolean firstPermissionCheck = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocationLogTv = findViewById(R.id.tv_location_tracker);
        mLocationLogTv.setMovementMethod(new ScrollingMovementMethod());
        Timber.e("onCreate");
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mainReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mainReceiver, new IntentFilter(PERMISSION_GRANTED));
        if(firstPermissionCheck)  checkPermissions();
        firstPermissionCheck = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionsGiven = 0;
        for (int result : grantResults)
            if (result == PackageManager.PERMISSION_DENIED) checkPermissions();
            else permissionsGiven++;
        Timber.e("all permissions given? %s", permissionsGiven == grantResults.length);
        if (permissionsGiven != grantResults.length) checkPermissions();
        else sendBroadcast(new Intent("PERMISSIONS_GRANTED"));
    }

    public void checkPermissions() {
        Timber.e("Checking permissions");
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
        };

        List<String> list = new ArrayList<String>();
        for (String permission : permissions)
            if (!(ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED))
                list.add(permission);

        String[] newPermissions = new String[list.size()];
        list.toArray(newPermissions);
        Timber.e("permission list size %s", list.size());
        if (list.size() > 0) ActivityCompat.requestPermissions(this, newPermissions, 1);
        else sendBroadcast(new Intent(PERMISSION_GRANTED));
    }
}