package com.example.root.fusedlocationtestapplication;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import timber.log.Timber;

import static com.example.root.fusedlocationtestapplication.Util.CHANNEL_TRACKING_ID;

public class TrackingApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        MultiDex.install(this);
        createNotificationChannel();
    }

    @SuppressLint("NewApi")
    private void createNotificationChannel() {
        if(!Util.isAndroidO()) return;

        NotificationChannel trackingChannel = new NotificationChannel(CHANNEL_TRACKING_ID, getString(R.string.title_channel_notification_tracking), NotificationManager.IMPORTANCE_DEFAULT);
        trackingChannel.setDescription(getString(R.string.description_channel_notification_tracking));
        NotificationManager manager = getSystemService(NotificationManager.class);
        if (manager != null) manager.createNotificationChannel(trackingChannel);
    }
}
