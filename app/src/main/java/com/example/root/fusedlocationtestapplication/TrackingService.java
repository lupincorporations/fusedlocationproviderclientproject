package com.example.root.fusedlocationtestapplication;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import java.text.SimpleDateFormat;

import timber.log.Timber;

public class TrackingService extends Service {

    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private AlarmManager mAlarmManager;
    private long INTERVAL = 2 * 1000 * 60;
    String locationTrackingLog = "Locations so far:\n\n";
    private BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.e( "onReceive");
            if(intent.getAction() != null){
                if(intent.getAction().equals(Util.SERVICE_ALARM_INTENT)){
                    Timber.e( "intent.getAction().equals(\"Tracking.Intent\")");
                    saveOnLog(getTimeNowFormatted()+" -> Time when the broadcast from AlarmManager was received\n");
                    startLocationUpdates();
                    setTrackingAlarm();
                }
            }
        }
    };
    private int TRACKING_ID_FOREGROUND_SERVICE = 9001;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.e( "onStartCommand");
        saveOnLog(getTimeNowFormatted()+" -> Service onStartCommand\n\n");
        setTrackingAlarm();
        setLocationSettings();
        setLocationRequest();
        return Service.START_STICKY;

    }

    private void saveOnLog(String messageToSave) {
        Timber.e( "saveOnLog\n%s", messageToSave);
        Util.saveOnLogFile(messageToSave);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.e("onCreate");
        saveOnLog(getTimeNowFormatted()+" -> Service onCreate\n\n");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) startForeground(TRACKING_ID_FOREGROUND_SERVICE, Util.getNotification(this));
        registerReceiver(serviceReceiver, new IntentFilter(Util.SERVICE_ALARM_INTENT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.e("onDestroy");
        saveOnLog(getTimeNowFormatted()+" -> Service onDestroy\n\n");
        unregisterReceiver(serviceReceiver);
        stopForeground(true);
        stopLocationUpdates();
        mAlarmManager.cancel(getServicePendingIntent());
    }

    private void setLocationSettings() {
        Timber.e("setLocationSettings");
        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);
    }

    @SuppressLint("NewApi")
    private void setTrackingAlarm() {
        Timber.e("setTrackingAlarm");
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        try {
            mAlarmManager.cancel(getServicePendingIntent());
        }
        catch (Exception e){
            Timber.e("Exception while canceling mAlarmManager: %s", e.getLocalizedMessage());
        }
        mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + INTERVAL, getServicePendingIntent());
    }

    private PendingIntent getServicePendingIntent() {
        return PendingIntent.getBroadcast(this, 666, new Intent(Util.SERVICE_ALARM_INTENT), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void setLocationRequest() {
        Timber.e("setLocationRequest");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
    }

    // Trigger new location updates at interval
    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        Timber.e("startLocationUpdates");

        // Create the location request to start receiving updates

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Timber.e("onLocationResult");
                Timber.e("locationResult: %s", locationResult.getLastLocation());
                onLocationChanged(locationResult.getLastLocation());
            }
        };
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        saveOnLog(getTimeNowFormatted()+" -> mFusedLocationClient.requestLocationUpdates\n\n");
    }

    public void onLocationChanged(Location location) {
        Timber.e("onLocationChanged");
        saveOnLog(getTimeNowFormatted()+" -> Time when onLocationResult returned value\n");
        locationTrackingLog = formatDate(location.getTime()) + " -> Location.getTime\n  **  LatLng: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude()) + " **  \n\n -- \n\n";
        saveOnLog(locationTrackingLog);
        stopLocationUpdates();
    }

    private String formatDate(long time) {
        return new SimpleDateFormat("HH:mm:ss").format(time);
    }

    private String getTimeNowFormatted() {
        return formatDate(System.currentTimeMillis());
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
